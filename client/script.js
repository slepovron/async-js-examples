// basic async events
setTimeout(() => {
  console.log("Show this after 2 sec");
}, 2000);

// this is simple callback
const button = document.querySelector("button");
let counter = 0;
button.addEventListener("click", function () {
  counter++;
  button.textContent = `Clicked ${counter} time`;
});

// ajax request by jquery library
const { ajax } = $;
ajax("/posts", {
  success(posts) {
    console.log(posts);
  },
});

// Promises example
delay(5000) // calling function that will wait 5 seconds
  .then(() => {
    // calling chained function "then" that will started AFTER or THEN function "delay" and will use it result
    console.log("Show this after 5 seconds");
  })
  .then(() => {
    // next chained function that will wait 1 second
    return delay(1000);
  })
  .then(() => {
    // next chained function
    return 7;
  })
  .then((value) => {
    // next chained that will use result of previously function
    console.log({ value }); // show in console object "value"
  });

//GET request by using fetch
fetch("/comments")
  .then((result) => {
    return result.json();
  })
  .then((comments) => {
    console.log(comments);
  });

//POST request by using fetch
fetch("/comments", {
  method: "POST",
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    text: "new comment",
  }),
})
  .then((response) => {
    response.json();
  })
  .then((comment) => {
    console.log(comment);
  });

//async/await using example for GET and POST requests
main();
main();

async function main() {
  console.log("Start async function example");
  await delay(2000);
  console.log(await getComments());
  await delay(2000);
  const newComment = await createComment({
    text: "Created by async/await",
  });
  console.log(newComment);
  await delay(2000);
  console.log(await getComments());
  await delay(2000);
  console.log("End async function exapmle");
}

async function getComments() {
  const result = await fetch("/comments");
  const comments = await result.json();
  return comments;
}

async function createComment() {
  const result = await fetch("/comments", {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      text: "new comment",
    }),
  });
  const comment = await result.json();
  return comment;
}

//---------------------------
//DELAY FUNCTION FOR EXAMPLES
function delay(milliseconds) {
  // some function that renutning promise
  return new Promise((resolve, reject) => {
    // promise return timeout
    setTimeout(() => resolve(), milliseconds);
  });
}
